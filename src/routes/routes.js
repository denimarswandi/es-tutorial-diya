import Reflection from '../controllers/Reflection'



const router = (app)=>{
  app.post('/api/v1/reflections', Reflection.create),
  app.get('/api/v1/reflections', Reflection.getAll),
  app.get('/api/v1/reflections/:id', Reflection.getOne),
  app.put('/api/v1/reflections/:id', Reflection.update),
  app.delete('/api/v1/reflections/:id', Reflection.delete)
}

export default router
